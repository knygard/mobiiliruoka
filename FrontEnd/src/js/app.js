var app = angular.module('app', [
  'ngResource', 'ngSanitize', // Angular
  'http-auth-interceptor', 'ui.router', // 3rd party
  'rtControllers', 'rtStates', 'rtDirectives', 'rtAPI' // Kiitti
]);

/* CONFIG */

app.constant("APIUrl", 'http://127.0.0.1:8000');

app.config(function(APIUrl, $locationProvider) {

  $locationProvider
    .html5Mode(true)
    .hashPrefix('!');
});

app.run(function($rootScope) {

});
