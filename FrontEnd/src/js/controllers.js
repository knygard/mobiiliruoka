var module = angular.module('rtControllers', ['rtAPI']);

module.controller('MainController', function($scope, $location) {

});

module.controller('MenuController', function($scope, menuData) {
  $scope.menuData = menuData;
  $scope.filters = {};

  $scope.filter = function(key, value) {
    if ($scope.filters[key] == value)
      delete $scope.filters[key];
    else
      $scope.filters[key] = value;
  }
});
