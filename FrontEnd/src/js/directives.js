var module = angular.module('rtDirectives', ['rtControllers']);

module.directive('nav', function($location) {
  return {
    restrict: 'EAC',
    link: function(scope, element, attrs) {
      var links = element.find('a');
      var activeClass = attrs.activeClass || 'active';

      scope.$on('$stateChangeSuccess', function() {
        for (var i = 0; i < links.length; i++) {
          link = angular.element(links[i]);
          var pathRegex = new RegExp("\/?#?" + link[0].pathname, "i");
          if (pathRegex.test($location.path()))
            link.addClass(activeClass)
          else
            link.removeClass(activeClass);
        };
      });
    }
  };
});

module.directive('rtMenu', function() {
  return {
    restrict: 'EA',
    transclude: false,
    replace: false,
    templateUrl: '../templates/directive_menu.html',
    scope: {
      days: '=days',
      restaurant: '=restaurant'
    }
  }
});
