var module = angular.module('rtStates', ['ui.router',
                                         'rtControllers', 'rtAPI']);

module.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider
    .when('/', '/today')
    .otherwise('/');

  var tdir = '../templates/';

  $stateProvider
    .state('login', {
      abstract: true,
      templateUrl: tdir + 'loginmaster.html',
      url: '/login',
      controller: 'LoginController'
    })
    .state('login.login', {
      url: '',
      templateUrl: tdir + 'loginmaster.login.html'
    })
    .state('login.forgot', {
      url: '/forgot',
      templateUrl: tdir + 'loginmaster.forgot.html'
    })

    .state('master', {
      abstract: true,
      templateUrl: tdir + 'master.html'
    })

    .state('master.menuToday', {
      url: '/today',
      templateUrl: tdir + 'master.menu.html',
      resolve: {
        menuData: function(MenuAPI, $stateParams) {
          return MenuAPI.today();
        }
      },
      controller: 'MenuController'
    })
    .state('master.monday', {
      url: '/menu/:day',
      templateUrl: tdir + 'master.menu.html',
      resolve: {
        menuData: function(MenuAPI, $stateParams) {
          return MenuAPI.byDayName($stateParams.day);
        }
      },
      controller: 'MenuController'
    })
    .state('master.menuTomorrow', {
      url: '/tomorrow',
      templateUrl: tdir + 'master.menu.html',
      resolve: {
        menuData: function(MenuAPI, $stateParams) {
          return MenuAPI.today();
        }
      },
      controller: 'MenuController'
    })
});
