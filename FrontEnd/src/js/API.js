var module = angular.module('rtAPI', ['ngResource']);

module.factory('MenuAPI', function($resource, APIUrl) {
  var today = new Date();
  var resource = $resource(APIUrl + '/:lang/menu/:year/:month/:day',
    {
      'lang': 'fi',
      'year': today.getFullYear(),
      'month': today.getMonth()+1,
      'day': today.getDate()
    },
    { 'get': {method: 'GET', isArray: false} }
  );

  function getWeekDayByName(dayName) {

    var days = ["monday", "tuesday", "wednesday",
                "thursday", "friday", "saturday", "sunday"];
    var currentDate = new Date;
    var firstDay = currentDate.getDate() - currentDate.getDay() + 1; // fixed for monday being first day

    return firstDay + days.indexOf(dayName);
  };

  return {
    today: function today() {
      return resource.get();
    },
    byDate: function byDate(year, month, day) {
      return resource.get({
        'year': year,
        'month': month,
        'day': day
      });
    },
    byDayName: function byDayName(dayName) {
      return resource.get({
        'day': getWeekDayByName(dayName)
      });
    }
  }
});
