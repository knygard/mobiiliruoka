# Mobiiliruoka

Project to create new food menu application in Otaniemi.

The application will fetch menu data from restaurants and provide an API. Frontend is built based on AngularJS, which shows a responsive, mobile friendly menu.

---
Work in progress.

## Technology

Backend: Django

Frontend: AngularJS, SASS, Susy

Other: Grunt, Bower

## Screenshots

Preliminary screenshot of a development version

![](http://bytebucket.org/knygard/mobiiliruoka/raw/2a4273f1e0a69345015b68bc39630017c0d84422/screenshots/preliminary.png)

