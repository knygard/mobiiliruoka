# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Restaurant'
        db.create_table(u'mobiiliruoka_restaurant', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_fi', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('name_se', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('fetchname', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('restaurantChain', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mobiiliruoka.RestaurantChain'], null=True, blank=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('city_fi', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('city_en', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('city_se', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('opening_time', self.gf('django.db.models.fields.TimeField')(null=True, blank=True)),
            ('closing_time', self.gf('django.db.models.fields.TimeField')(null=True, blank=True)),
            ('url_fi', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('url_en', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('url_se', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('latitude', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=11, decimal_places=7, blank=True)),
            ('longitude', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=11, decimal_places=7, blank=True)),
            ('description_fi', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('description_en', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('description_se', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('keywords_fi', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('keywords_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('keywords_se', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, auto_now_add=True, null=True, blank=True)),
            ('date_modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, auto_now=True, null=True, blank=True)),
        ))
        db.send_create_signal(u'mobiiliruoka', ['Restaurant'])

        # Adding model 'RestaurantChain'
        db.create_table(u'mobiiliruoka_restaurantchain', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'mobiiliruoka', ['RestaurantChain'])

        # Adding model 'Meal'
        db.create_table(u'mobiiliruoka_meal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('restaurant', self.gf('django.db.models.fields.related.ForeignKey')(related_name='meals', to=orm['mobiiliruoka.Restaurant'])),
            ('content', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('lang', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('tags', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'mobiiliruoka', ['Meal'])


    def backwards(self, orm):
        # Deleting model 'Restaurant'
        db.delete_table(u'mobiiliruoka_restaurant')

        # Deleting model 'RestaurantChain'
        db.delete_table(u'mobiiliruoka_restaurantchain')

        # Deleting model 'Meal'
        db.delete_table(u'mobiiliruoka_meal')


    models = {
        u'mobiiliruoka.meal': {
            'Meta': {'ordering': "['date', 'restaurant', 'content']", 'object_name': 'Meal'},
            'content': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'restaurant': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'meals'", 'to': u"orm['mobiiliruoka.Restaurant']"}),
            'tags': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'mobiiliruoka.restaurant': {
            'Meta': {'ordering': "['slug']", 'object_name': 'Restaurant'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'city_en': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'city_fi': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'city_se': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'closing_time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'date_modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now': 'True', 'null': 'True', 'blank': 'True'}),
            'description_en': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'description_fi': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'description_se': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'fetchname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'keywords_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'keywords_fi': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'keywords_se': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '11', 'decimal_places': '7', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '11', 'decimal_places': '7', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_fi': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name_se': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'opening_time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'restaurantChain': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mobiiliruoka.RestaurantChain']", 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'url_en': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'url_fi': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'url_se': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'mobiiliruoka.restaurantchain': {
            'Meta': {'object_name': 'RestaurantChain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['mobiiliruoka']