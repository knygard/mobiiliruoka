from django.contrib import admin
from models import Campus, Restaurant, RestaurantChain, Meal

class RestaurantAdmin(admin.ModelAdmin):
	pass


class RestaurantChainAdmin(admin.ModelAdmin):
	pass


class MealAdmin(admin.ModelAdmin):
    list_display = ('content', 'tags', 'lang', 'date')


class CampusAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'city')


admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(RestaurantChain, RestaurantChainAdmin)

admin.site.register(Meal, MealAdmin)
admin.site.register(Campus, CampusAdmin)
