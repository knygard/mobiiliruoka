from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import get_language
from django.utils.safestring import mark_safe
from datetime import datetime, date
from easymode.i18n.decorators import I18n
import ast # ast = abstract syntax strees
import calendar


@I18n('name', 'description', 'city')
class Campus(models.Model):
    name = models.CharField(_('name'), max_length=255, blank=True)
    slug = models.SlugField(_('slug'), blank=True)
    description = models.TextField(_('description'), default='', blank=True)
    image = models.ImageField(_('image'), upload_to='campuses/', blank=True, null=True);

    city = models.CharField(_('city'), max_length=100, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('campus')
        verbose_name_plural = _('campuses')


@I18n('name', 'url', 'city', 'description', 'keywords')
class Restaurant(models.Model):

    name = models.CharField(_('name'), max_length=255, blank=True)
    fetchname = models.CharField(_('fetchname'), max_length=255, blank=True)
    restaurantChain = models.ForeignKey('RestaurantChain', verbose_name=_('restaurant chain'), blank=True, null=True)
    slug = models.SlugField(_('slug'), blank=True)
    address = models.CharField(_('address'), max_length=255, blank=True)
    city = models.CharField(_('city'), max_length=100, blank=True)
    campus = models.ForeignKey('Campus', verbose_name=_('campus'), blank=True, null=True)
    opening_time = models.TimeField(_('opening time'), blank=True, null=True)
    closing_time = models.TimeField(_('closing time'), blank=True, null=True)
    url = models.URLField(_('url'), blank=True)
    latitude = models.DecimalField(_('latitude'), max_digits=11, decimal_places=7, blank=True, null=True)
    longitude = models.DecimalField(_('longitude'), max_digits=11, decimal_places=7, blank=True, null=True)

    description = models.TextField(_('description'), default='', blank=True)
    keywords = models.TextField(_('keywords'), blank=True)

    image = models.ImageField(_('image'), upload_to='restaurants/', blank=True, null=True);

    date_added = models.DateTimeField(_('date added'), auto_now_add=True, default=datetime.now, blank=True, null=True)
    date_modified = models.DateTimeField(_('date modified'), auto_now=True, default=datetime.now, blank=True, null=True)

    def __unicode__(self):
    	return self.name

    class Meta:
        ordering = ['slug']
        verbose_name = _('restaurant')
        verbose_name_plural = _('restaurants')

    def open_time(self):

        f = Fetch.objects.filter(name=self.fetchname).order_by('-date_added')

        # Select (only) one item if query returned anything
        if f:
            f = f[0]
        else:
            return ''

        return f.opening_time


class RestaurantChain(models.Model):

    name = models.CharField(_('name'), max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('restaurant chain')
        verbose_name_plural = _('restaurant chains')



class Meal(models.Model):

    restaurant = models.ForeignKey(Restaurant, to_field='id', related_name='meals')
    content = models.CharField(max_length=255)
    lang = models.CharField(max_length=10)
    tags = models.CharField(max_length=64, blank=True)
    date = models.DateField()
    timestamp = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.content

    class Meta:
        ordering = ['date', 'restaurant', 'content']
        verbose_name = _('meal')
        verbose_name_plural = _('meals')

    @property
    def tags_list(self):
        return [tag.strip() for tag in self.tags.split(",")]

