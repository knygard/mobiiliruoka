from django.conf.urls import patterns, include, url
from mobiiliruoka.API.MenuAPI import MenuAPI

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('',
    url(r'^(?P<lang>\w{2})/menu/(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/?$', MenuAPI.as_view()),
)
