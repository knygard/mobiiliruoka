# -*- coding: utf-8 -*-

import datetime

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import exceptions as exc

from mobiiliruoka.models import Campus, Restaurant, Meal
from mobiiliruoka.serializers import RestaurantSerializerGETMany
from mobiiliruoka.exceptions import NotFound


class MenuAPI(APIView):

    def get(self, request, lang='en',
            year=None, month=None, day=None):

        campus = None
        if request.GET.get('campus', None):
            try:
                campus = Campus.objects.get(slug=request.GET.get('campus'))
            except Campus.DoesNotExist:
                pass

        if year and month and day:
            date = datetime.date(int(year), int(month), int(day))
        else:
            raise exc.ParseError("Year, month and day needed")

        return self.get_many(request, date, lang, campus)

    def get_many(self, request, date, lang, campus=None):

        restaurants = Restaurant.objects.all()
        if campus:
            restaurants = restaurants.filter(campus=campus)

        context = {
            'date': date,
            'language': lang
        }

        serializer = RestaurantSerializerGETMany(restaurants, many=True, context=context)

        response = {
            "language": 'fi',
            "restaurants": serializer.data
        }

        return Response(response, 200)
