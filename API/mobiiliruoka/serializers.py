from rest_framework import serializers
from rest_framework import status
from rest_framework import exceptions as exc

from mobiiliruoka.models import Restaurant, Meal

import datetime


class MealSerializerGETMany(serializers.ModelSerializer):

    tags = serializers.SerializerMethodField('get_tags')

    class Meta:
        model = Meal
        fields = ('content', 'tags')

    def get_tags(self, obj):
        return obj.tags_list


class RestaurantSerializerGETMany(serializers.ModelSerializer):

    name = serializers.SerializerMethodField('get_name')
    description = serializers.SerializerMethodField('get_description')
    city = serializers.SerializerMethodField('get_city')
    menus = serializers.SerializerMethodField('get_menus')

    class Meta:
        model = Restaurant
        fields = ('name', 'description', 'address', 'city',
                  'url_fi', 'url_en', 'menus')

    def get_menus(self, obj):

        if self.context.get('date', None):
            date = self.context.get('date')
        else:
            date = datetime.date.today()

        if self.context.get('language', None):
            language = self.context.get('language')
        else:
            language = 'en'

        meals = obj.meals.filter(lang=language, date=date)

        asd = {
            'date': date,
            'day': date.today().strftime("%A"),
            'meals': MealSerializerGETMany(meals, many=True).data
        }

        return [asd]

    def get_attr_by_lang(self, obj, attr):
        if not self.context.get('language', None):
            raise exc.ParseError("Language needed.")
        return getattr(obj, str(attr) + '_' + self.context.get('language'))

    def get_name(self, obj):
        return self.get_attr_by_lang(obj, 'name')

    def get_description(self, obj):
        return self.get_attr_by_lang(obj, 'description')

    def get_city(self, obj):
        return self.get_attr_by_lang(obj, 'city')
