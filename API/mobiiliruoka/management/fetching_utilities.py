from mobiiliruoka.management.fetching_config import *
from mobiiliruoka.models import Meal
import re # regex

def parsedMeal(meal):
    """ Returns a adictionary with tags and meal without tags """

    # find tags between parenthesis (...)
    found_tags = re.findall(r'\((.*?)\)', meal)

    # reformat tags
    tags = []
    for tag in found_tags:
        tmp = tag.split(",")
        for t in tmp:
            tags.append(t.strip())

    # include only tags with 1-2 characters
    tags = [tag for tag in tags if len(tag) <= 2]

    for tag in tags:
        # remove if probably not a real tag
        if len(tag) > 2:
            print "removed: %s with length %d" % (tag, len(tag))
            tags.remove(tag)

    if tags:
        # remove tags from meals
        for found in found_tags:
            # remove only if it is a tag and not a description like "(indian food)"
            if len(found) <= 2 or found.strip(",") > 1:
                meal = meal.replace('('+ found +')', '')

    # now find tags without parenthesis, e.g. 'gl v m'
    for word in meal.split(" "):
        if len(word) <= 2 and len(word) > 0:
            w = word.upper()
            if w in ALLOWED_TAGS:
                tags.append(w)
                # remove from meal where string is in the end
                if meal[-(len(w)+1):] == ' '+word:
                    meal = meal.replace(' '+word, '')
                # remove from meal where string is in the middle
                meal = meal.replace(' '+word+' ', '')

    # clean
    meal = meal.strip()

    return {'meal': meal, 'tags': tags}


def removeExistingMealsByDate(restaurant, date, lang):
        meals = Meal.objects.filter(restaurant=restaurant, date=date, lang=lang)
        meals.delete()
