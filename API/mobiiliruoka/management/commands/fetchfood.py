from mobiiliruoka.management.fetching_config import *
from mobiiliruoka.management.fetching_utilities import *
from django.core.management.base import BaseCommand, CommandError
from mobiiliruoka.models import Meal, Restaurant
import urllib2
import ujson
import datetime

class Command(BaseCommand):

    args = 'none'
    help = 'Fetches the restaurant info from lounasaika.net'

    def handle(self, *args, **options):

        req = urllib2.Request("http://www.lounasaika.net/api/v1/menus.json")
        opener = urllib2.build_opener()
        f = opener.open(req)
        json_data = ujson.loads(f.read())
        # json_data=open('menus.json') # debug

        date_now = datetime.date.today()
        current_week = date_now.isocalendar()[1]
        current_weekday = date_now.isocalendar()[2] # 1-7 Mon-Sun
        date_monday = date_now + datetime.timedelta(-current_weekday)  # timedelta in days

        for restaurant in json_data:

            # Skip non-Otaniemi restaurants
            # if restaurant['campus'] not in enabled_campuses:
            #     self.stdout.write('Fetching skipped for restaurant: "%s"' % restaurant['name'])
            #     continues

            # Skip immediately if 'meals' key not found
            if not restaurant.has_key('meals'): continue

            # Skip immediately if restaurant has no name
            if not restaurant.has_key('name'): continue

            # Get existing restaurant
            try:
                tempRestaurant = Restaurant.objects.get(fetchname=restaurant['name'])
            # Create new restaurant
            except Restaurant.DoesNotExist:
                newrestaurant = Restaurant()
                newrestaurant.fetchname = restaurant['name']
                newrestaurant.name_fi = restaurant['name']
                newrestaurant.name_en = restaurant['name']
                newrestaurant.name_se = restaurant['name']
                if restaurant.has_key('address'):
                    newrestaurant.address = restaurant['address']
                if restaurant.has_key('url.fi'):
                    newrestaurant.url_fi = restaurant['url']['fi']
                if restaurant.has_key('url.en'):
                    newrestaurant.url_en = restaurant['url']['en']
                newrestaurant.save()
                tempRestaurant = newrestaurant


            for lang in restaurant['meals']:

                # Ensure that there is a list of meals
                if not isinstance(restaurant['meals'][lang], list): continue
                for day in restaurant['meals'][lang]:
                    weekday = restaurant['meals'][lang].index(day) + 1 # 1-7 Mon-Sun
                    date = date_monday + datetime.timedelta(weekday)
                    # Remove existing meals by date
                    removeExistingMealsByDate(restaurant=tempRestaurant.pk, date=date, lang=lang)
                    for food in day:
                        # skip if no menu or no content
                        if not food or len(food) < 5: continue
                        if food in DISALLOWED_CONTENT: continue
                        # Create meal
                        meal = Meal()
                        meal.content = food
                        meal.tags = ", ".join(parsedMeal(food)['tags'])
                        meal.lang = lang
                        meal.date = date
                        meal.restaurant_id = tempRestaurant.id
                        meal.save()

            self.stdout.write('Fetched food for restaurant: "%s"' % restaurant['name'])
