from django.shortcuts import render
from django.http import Http404, HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from mobiiliruoka.models import Restaurant, Meal
import datetime
from isoweek import Week


def getMeals(startdate, enddate, lang):
    """
    Returns list (Django ORM) of meals between given dates by language
    """
    return Meal.objects.filter(date__gte=startdate, date__lte=enddate, lang=lang)


def getMealsByRestaurants(startdate, enddate, lang):
    """
    Returns list of restaurants with meals
    """
    meals = getMeals(startdate, enddate, lang)
    restaurants = Restaurant.objects.all()
    data = []

    for restaurant in restaurants:
        meals_by_restaurant = meals.filter(restaurant=restaurant)
        menus = []

        # iterate over days
        iterdate = startdate
        delta = datetime.timedelta(days=1)
        while iterdate <= enddate:
            menu = {
                'day': iterdate.strftime("%A"),
                'date': iterdate,
                'meals': [tempmeal.serialize() for tempmeal in meals_by_restaurant.filter(date=iterdate)]
            }
            menus.append(menu)
            iterdate += delta

        # Unite dictionaries
        data.append(dict(
            {'menus': menus}.items() + restaurant.serialize().items()
        ))

    return data


class MealsThisWeek(APIView):

    def get(self, request, lang='en'):
        data = getMealsByRestaurants(Week.thisweek().monday(), Week.thisweek().sunday(), lang)
        return Response({"restaurants": data, "language": lang}, 200)


class MealsByDate(APIView):

    def get(self, request, year, month, day, lang='en'):
        date = datetime.date(int(year), int(month), int(day))
        data = getMealsByRestaurants(date, date, lang)
        return Response({"restaurants": data, "language": lang}, 200)


def index(request):
    return render(request, 'index.phtml')

def menu(request, year=None, month=None, day=None):
    restaurants = Restaurant.objects.all()
    date = datetime.date(year, month, day)

    for restaurant in restaurants:
        restaurant.menu = restaurant.menu_by_day(day)

    return render(request, 'menu.phtml', {'restaurants': restaurants, 'day': days[day]})

def menu_old(request, day=datetime.date.today().strftime("%A")):
    restaurants = Restaurant.objects.all()

    day = day.lower()
    days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
    day = days.index(day)

    for restaurant in restaurants:
        restaurant.menu = restaurant.menu_by_day(day)

    return render(request, 'menu.phtml', {'restaurants': restaurants, 'day': days[day]})

def restaurant(request, restaurant):
    restaurant = Restaurant.objects.get(slug=restaurant.lower())
    menu = restaurant.menu_today()
    return render(request, 'restaurant.phtml', {'restaurant': restaurant, 'menu': menu})

def restaurant_image(request, restaurant):
    return render(request, 'restaurant.phtml')

def restaurants(request):
    objlist = Restaurant.objects.all()
    return render(request, 'restaurants.phtml', {'restaurants': objlist})
