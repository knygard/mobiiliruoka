# API

Work in progress

	/api/restaurants        # returns a list of restaurant objects
	/api/restaurant/:id     # returns a restaurant object by id
	/api/menu               # returns a list of meal objects for this week
	/api/menu/:day          # returns a list of meal objects by day
